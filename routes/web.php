<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\SendEmailTest;
use App\Http\Controllers\BookController;
use App\Http\Controllers\AddController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\IssueBookController;
use Illuminate\Support\Facades\Auth;

// use Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// //login to admin panel
 Route::post('login', [UserController::class, 'LogIn'])->name('login')->middleware('authcheck');
Route::get('register', [UserController::class, 'register']);
Route::post('register1', [UserController::class, 'register1'])->name('register1');
//routes of all book details
Route::post('book/add', [BookController::class, 'add'])->name('book/add')->middleware('authcheck');
Route::get('book/show', [BookController::class, 'show'])->name('book/show')->middleware('authcheck');
Route::get('book/delete/{id}', [BookController::class, 'delete'])->name('book.delete')->middleware('authcheck');
Route::get('book/edit/{id}', [BookController::class, 'edit'])->name('book.edit')->middleware('authcheck');
Route::post('book/edit1/{id}', [BookController::class, 'edit1'])->name('book/edit1')->middleware('authcheck');
Route::get('bookpdf',[BookController::class, 'pdf']);
//routes of all auther details
Route::post('auther/add1', [AddController::class, 'add1'])->name('auther/add1')->middleware('authcheck');
Route::get('auther/show', [AddController::class, 'show'])->name('auther/show')->middleware('authcheck');
Route::get('auther/delete/{id}', [AddController::class, 'delete'])->name('auther.delete')->middleware('authcheck');
Route::get('auther/edit/{id}', [AddController::class, 'edit'])->name('auther.edit')->middleware('authcheck');
Route::post('auther/edit1/{id}', [AddController::class, 'edit1'])->name('auther/edit1')->middleware('authcheck');
//routes of all issue_book details
Route::post('issue/add', [IssueBookController::class, 'add'])->name('issue/add')->middleware('authcheck');
Route::get('issue/show', [IssueBookController::class, 'show'])->name('issue.show')->middleware('authcheck');
Route::get('issue/delete/{id}', [IssueBookController::class, 'delete'])->name('issue.delete')->middleware('authcheck');
Route::get('issue/edit/{id}', [IssueBookController::class, 'edit'])->name('issue.edit')->middleware('authcheck');
Route::post('issue/edit1/{id}', [IssueBookController::class, 'edit1'])->name('issue/edit1')->middleware('authcheck');
Route::get('changeStatus',[IssueBookController::class, 'status']);
Route::get('pdf/{id}',[IssueBookController::class, 'pdf']);
//routes of all client details
Route::post('client/add', [ClientController::class, 'add'])->name('client/add')->middleware('authcheck');
Route::get('client/show', [ClientController::class, 'show'])->name('client/show')->middleware('authcheck');
Route::get('client/delete/{id}', [ClientController::class, 'delete'])->name('client.delete')->middleware('authcheck');
Route::get('client/edit/{id}', [ClientController::class, 'edit'])->name('client.edit')->middleware('authcheck');
Route::post('client/edit1/{id}', [ClientController::class, 'edit1'])->name('client/edit1')->middleware('authcheck');
//routes of sidebar to book
Route::get('home', [UserController::class, 'index'])->name('home')->middleware('authcheck');
//routes of sidebar to auther
Route::get('auther', [UserController::class, 'index1'])->name('auther')->middleware('authcheck');
//routes of sidebar to issue_book
Route::get('issue', [UserController::class, 'index2'])->name('issue')->middleware('authcheck');
//route of sidebar to client
Route::get('client', [UserController::class, 'index3'])->name('client')->middleware('authcheck');
//route of sidebar to BookType
Route::get('booktype', [UserController::class, 'index4'])->name('booktype')->middleware('authcheck');
//route of sidebar to BookType
Route::get('logout', [UserController::class, 'index5'])->name('logout');
Route::get('authlogin', [UserController::class, 'index6'])->name('authlogin');
Auth::routes();

Route::get('/library', 'HomeController@index')->name('library');
