<?php

namespace App\Console;

use App\Console\Commands\penalty;
use App\Console\Commands\penaltymail;
use App\Console\Commands\SendDocLinkToUsers;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SendDocLinkToUsers::class,
        penalty::class,
        penaltymail::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('reminder:emails')->daily()->at('08:00');
        $schedule->command('penalty:send')->daily()->at('08:00');
        $schedule->command('penaltysend:mail')->daily()->at('08:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
