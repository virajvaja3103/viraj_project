<?php

namespace App\Console\Commands;
use App\issue_book;
use Illuminate\Console\Command;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Collection;

class penalty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'penalty:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $issue_data=issue_book::whereDate('return_date','>=',Carbon::now())->where('status',1)->with(['clients'])->get();

        foreach($issue_data as $t)
        {
            $return_date=$t->return_date;
            $today=Carbon::now();
            $return_date= new DateTime($return_date);
            $today= new DateTime($today);
            $days= $return_date->diff($today);
            $days=$days->format('%a');
            $penalty_amount=$days*100;
            echo $penalty_amount;
            $final=issue_book::where('id',$t->id)->update(['penalty'=>$penalty_amount]);

        }
    }
}
