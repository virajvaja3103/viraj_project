<?php

namespace App\Http\Controllers;

use App\book;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\CustomClasses\ColectionPaginate;
use Illuminate\Support\Collection;
use Validator,Redirect,Response;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use PDF;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $data = request()->validate([
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
            'price' => 'required|numeric',
            'auther_id' => 'required'
            ]);
        if($data)
        {
        $name = $request->input('name');
        $price = $request->input('price');
        $auther_id = $request->auther_id;
        $data=array('name'=>$name,"price"=>$price,'auth_id'=>$auther_id);
        $d=book::insert($data);
        if($d)
        {
            // $googleAPIToken = env('GOOGLE_API_TOKEN');
            // dd($googleAPIToken);
            return back()->withFlashSuccess("inserted");
        }
        else
        {
            dd("Not successfully add");
        }
        }
        else
        {
            dd("Not successfully validated");
        }
    }
    public function show()
    {
        $book = book::with(['author'])->get();
        return view('show')->with('book',$book);

    }

    public function delete(Request $request,$id)
    {
        if(!Auth::check())
        {
          return view('error');
        }
        else
        {
        $book=book::where('id',$id)->delete();
        if($book)
        {
            return redirect()->route('book/show')->with('success','successfully deleted');
        }
        else
        {
            dd("Not successfully deleted");
        }
        }
    }
    public function pdf(Request $request)
    {
        $book=book::with(['author'])->get();
        $image_url = public_path().'/image'.'/'.'book.jpg';
        $image = file_get_contents($image_url);
        if ($image !== false){
            $base64 = 'data:image/jpg;base64,'.base64_encode($image);
        }
        $data = [
            'book' => $book,
            'image' => $base64
        ];
        $pdf = PDF::loadView('bookpdf',['data' => $data]);
        $pdf->setPaper('L', 'landscape');
        $pdf->setOptions(['isPhpEnabled' => true]);
        $return= $pdf->download('bookdata.pdf');
        return $return;
    }
    public function edit($id)
    {
        $users = book::where('id',$id)->first();
        return view('bookedit')->with('users',$users);
    }
    public function edit1(Request $request,$id)
    {
        $update=book::where('id', $id)->update(['name'=>$request['name'],'price'=>$request['price']]);
        if($update)
        {
            return back()->withFlashSuccess("updated");
        }
        else
        {
            dd("Not successfully updated");
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(book $book)
    {
        //
    }
}
