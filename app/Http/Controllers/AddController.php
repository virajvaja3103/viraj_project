<?php

namespace App\Http\Controllers;

use App\add;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function add1(Request $request)
    {
        if(!Auth::check())
        {
          return view('error');
        }
        else
        {
        $data = request()->validate([
            'auther_name' => 'required|regex:/^[\pL\s\-]+$/u|max:255'
            ]);

        if($data)
        {
        $name = $request->input('auther_name');
        $data=array('name'=>$name);
        $d=add::insert($data);
        if($d)
        {
            return view('auther');
        }
        else
        {
            dd("Not succeesfully added");
        }
        }
        else
        {
            dd("Not succeesfully validated");
        }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\add  $add
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if(!Auth::check())
        {
          return view('error');
        }
        else
        {
        $auther = add::get();
        return view('authershow')->with('auther',$auther);
        }
    }

    public function delete(Request $request,$id)
    {
        if(!Auth::check())
        {
          return view('error');
        }
        else
        {
        $add=add::where('id',$id)->delete();
        if($add)
        {
            return redirect()->route('auther/show')->with('success','successfully deleted');
        }
        else
        {
            dd("Not succeesfully deleted");
        }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\add  $add
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $auther = add::where('id',$id)->first();
        return view('autheredit')->with('auther',$auther);

    }
    public function edit1(Request $request,$id)
    {
        if(!Auth::check())
        {
          return view('error');
        }
        else
        {
       $update=add::where('id', $id)->update(['name'=>$request['auther_name']]);
        if($update)
        {
            return back()->withFlashSuccess("updated");
        }
        else
        {
            dd("Not succeesfully updated");
        }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\add  $add
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, add $add)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\add  $add
     * @return \Illuminate\Http\Response
     */
    public function destroy(add $add)
    {
        //
    }
}
