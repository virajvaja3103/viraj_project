<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\pdfmail;

class pdfmaker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $user;
    // public $pdf;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user,$pdf)
    {
        $this->user=$user;
        // $this->pdf=$pdf;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user=$this->user;
       // $pdf=$this->pdf;
        $email = new pdfmail($user);
        Mail::to($user->clients['email'])->send($email);
    }
}
