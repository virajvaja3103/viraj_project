<!DOCTYPE html>
<html>
<head>
	<title>Details About Issue Book Details</title>
</head>
<body>

   <div class="content-wrapper">
        <table class="table table-bordered table-striped" style="margin: 1% 0 0 1%; width:95%;">
            <thead>
            <th>issue date</th>
            <th>Return date</th>
            <th>creatd_at</th>
            <th>update_at</th>
            <th>book_name</th>
            <th>client_name</th>
            </thead>
             <tbody>
            @foreach($book as $value)
            <tr>
            <td>{{ $value['issue_date'] }}</td>
            <td>{{ $value['return_date'] }}</td>
            <td>{{ $value['created_at']}}</td>
            <td>{{ $value['updated_at']}}</td>
            <td>{{ $value->book_details['name']}}</td>
            <td>{{ $value->clients['name']}}</td>
            @endforeach
</body>
</html>
