<!DOCTYPE html>
<html>
<head>
	<title>Details About Issue Book Details</title>
    <style>
        table td, table th{
            border:1px solid black;
            font-size: 26px;
            border-collapse: collapse;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
            background-color: #009879;
            border-color: black;
        }
    </style>

</head>
<body>
    <header style="position: fixed;top:-19px; left:370px">
            <div>
                <img src="{{ $data['image'] }}" style="width: 20px; height: 20px">
            </div>
    </header>
   <div class="content-wrapper">
        <table class="table table-bordered table-striped" style="margin: 1% 0 0 1%; width:95%;">
            <thead>
            <th>Book Name</th>
            <th>Book Price</th>
            <th>Auther Name</th>
            </thead>
             <tbody>
            @foreach($data['book'] as $value)
            <tr>
            <td>{{ $value['name'] }}</td>
            <td>{{ $value['price'] }}</td>
            <td>{{ $value->author['name']}}</td>
            @endforeach
        </table>
   </div>
   <script type="text/php">
    if ( isset($pdf) ) {
        $x1 = 72;
        $y1 = 18;
        $text1 = "Details About Book Details";
        $png_url = "product-".time().".png";
        {{-- $image="{{$data['image']}}"; --}}
        $font1 = $fontMetrics->get_font("helvetica", "bold");
        $size1 = 18;
        $color1 = array(0,0,0);
        $word_space1 = 0.0;  //  default
        $char_space1 = 0.0;  //  default
        $angle1 = 0.0;   //  default
        $pdf->page_text($x1, $y1, $text1, $font1, $size1, $color1, $word_space1, $char_space1, $angle1);
        $x = 410;
        $y = 580;
        $text = "{PAGE_NUM} of {PAGE_COUNT}";
        $font = $fontMetrics->get_font("helvetica", "bold");
        $size = 14;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>
</body>
</html>
